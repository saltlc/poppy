=====
Poppy
=====

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/


A simple cpop example


What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/vmware/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/vmware/pop/pop-create/>`__

Getting Started
===============

Prerequisites
-------------

* Python 3.10+
* git *(if installing from source, or contributing to the project)*

Installation
------------

.. note::

   If wanting to contribute to the project, and setup your local development
   environment, see the ``CONTRIBUTING.rst`` document in the source repository
   for this project.

If wanting to use ``poppy``, you can do so by either
installing from PyPI or from source.


Install from source
-------------------

.. code-block:: bash

   # clone repo
   git clone git@saltlc/poppy.git
   cd poppy

   # Setup venv
   python3 -m venv .venv
   source .venv/bin/activate
   pip install .

Usage
=====


Invoke Poppy's CLI

.. code-block:: bash

   hub poppy.init.cli

You can also invoke the hub as a python module:

.. code-block:: bash

   python -m hub poppy.init.cli


View the cli help for poppy:

.. code-block:: bash

   python -m hub poppy.init.cli --help

Save the poppy command

.. code-block:: bash

   alias poppy='python -m hub poppy.init.cli'

Roadmap
=======

Reference the `open issues <https://gitlab.com/Akm0d/salt-agi/issues>`__ for a list of
proposed features (and known issues).

Acknowledgements
================

* `Img Shields <https://shields.io>`__ for making repository badges easy.
