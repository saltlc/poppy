async def cli(hub):
    if hub.OPT.pop.subparser:
        print(f"Subparser '{hub.OPT.pop.subparser}' works!")
    else:
        print("Poppy works!")
